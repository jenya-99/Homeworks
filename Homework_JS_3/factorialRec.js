// Вариант с рекурсией

function calcFactRec(n) {
 return (n<0 || parseInt(n)!=n)? NaN:((n==0 || n==1)? 1: n*calcFactRec(n-1));
}
let namber = prompt('Введите число');
alert(`${namber}! = ${calcFactRec(namber)}`);

//Пробелы в стр.4 поубирал специально, потому что строка длинная
//мне кажется это лучше чем перенос строки.