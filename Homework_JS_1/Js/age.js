//получаем имя от пользователя и проверяем на null и пустую строку
//с помощью function checkString():
let userName = prompt('Введите Ваше имя', '');
userName = checkString(userName);
if (userName == '' || userName == null) {
  alert("Вы отказались ввести имя, до свидания.");
  throw "Имя не введено"; //прерывает дальнейшее выполнение кода   
}

//получаем возраст от пользователя и проверяем на число
//с помощью function checkNumber():
let userAge = prompt('Введите Ваш возраст', '');
userAge = checkNumber(userAge);
if (!userAge) {
  alert("Вы отказались ввести возраст, до свидания.");
  throw "Возраст не введен"; //прерывает дальнейшее выполнение кода 
}

//анализируем полученный возраст и выводим результат:
if (userAge < 18) alert('You are not allowed to visit this website');
else if (userAge > 22) alert('Welcome, ' + userName);
else {
  let answer = confirm('Are you sure you want to continue?');
  if (answer) alert('Welcome, ' + userName);
  else alert('You are not allowed to visit this website'); 
}