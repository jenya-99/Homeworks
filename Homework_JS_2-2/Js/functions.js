// Фунция проверки на простоту числа n:
function checkPrim (n){
  if (n < 2) return false;// 0 и 1 - не являются простыми числами
  else if (n % 2 == 0 && n != 2) return false;//если четное - значит cоставное    
  else { 
    for (let i = 3; i < Math.sqrt(n); i+=2){//перебираем только нечетные делители до √n (дальше нет смысла)        
      if (n % i == 0) return false;//если нечетное составное - вернет false
    }
    return true;// вернет true, когда 2 или другое простое число
  }
}