function clonObject(obj) {
  let newObj = {};
  if (Array.isArray(obj) === true) {
    newObj = [];  // на случай, если попадется массив
  }
  for (let key in obj) {
    if (typeof(obj[key]) != 'object'){
      newObj[key] = obj[key];
    }  
    else newObj[key] = clonObject(obj[key]); 
  }
  return newObj;
}