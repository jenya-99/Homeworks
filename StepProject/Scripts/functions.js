//Toggle Aсtive Elements:
function toggleAсtiveElem(elements, clickElem) {
  for(let i = 0; i < elements.length; i++) {
    elements[i].classList.remove('active');
  }
  clickElem.classList.add('active');
}
//Добавляет картинки по qntImg шт. при двух кликах на elem: 
function addImg(qntImg, elem) {  
  preloadId.style.display = "none";  
  if (!elem.classList.contains('onceAdded')) {
    for(let i = 0; i < qntImg; i++) {  
      let newDiv = document.createElement('DIV');
      newDiv.classList.add('amazing-work-list-item');
      newDiv.innerHTML = document.getElementsByClassName('amazing-work-list-item')[0].innerHTML;
      document.getElementsByClassName("amazing-work-list")[0].appendChild(newDiv);
      let newImg = document.getElementsByClassName("amazing-work-list-item-img")[0];
      newImg.setAttribute('src',`Images/Amazing-work/Next-img/next-${i}.jpg`);
      elem.classList.add('onceAdded');
    }
  }
  else {
    for(let i = 0; i < qntImg; i++) {  
      let newDiv = document.createElement('DIV');
      newDiv.classList.add('amazing-work-list-item');
      newDiv.innerHTML = document.getElementsByClassName('amazing-work-list-item')[0].innerHTML;
      document.getElementsByClassName("amazing-work-list")[0].appendChild(newDiv);
      let newImg = document.getElementsByClassName("amazing-work-list-item-img")[0];
      newImg.setAttribute('src',`Images/Amazing-work/Next-2-img/next-${i}.jpg`);
      elem.classList.add('onceAdded');
    }
    document.getElementsByClassName('amazing-work-btn')[0].style.visibility = 'hidden';
  }
}
// Создает объект с картинками по категориям Amazing Work:
function CreatImgObj(qnt) {
  this['Graphic Design'] = [];
  this['Web Design'] = [];
  this['Landing Pages'] = [];
  this.Wordpress = [];
  this.All = []; 
  for(let i = 0; i < qnt; i++) {
    this['Graphic Design'].push(`Images/GraphicDesign/${i}.jpg`);
    this['Web Design'].push(`Images/WebDesign/${i}.jpg`);
    this['Landing Pages'].push(`Images/LandingPage/${i}.jpg`);
    this.Wordpress.push(`Images/Wordpress/${i}.jpg`);
    this.All.push(`Images/GraphicDesign/${i}.jpg`,
                  `Images/WebDesign/${i}.jpg`,
                  `Images/LandingPage/${i}.jpg`,
                  `Images/Wordpress/${i}.jpg`
                  );
  } 
}
// Show Person data:
function showPerson(elem) { 
  personImgId.children[0].src = dataPersons[elem.dataset.img].urlImg;
  personNameId.textContent = dataPersons[elem.dataset.img].name;
  personPositionId.textContent = dataPersons[elem.dataset.img].position;
  personTextId.textContent = dataPersons[elem.dataset.img].text;
}
// Add images to best-images section:
function addBestImages(qntImg) {  
  preloadId.style.display = "none";
  let columns = document.getElementsByClassName('best-images-list-column');
  for(let j = 1; j < qntImg; j++) {
    for(let i = 0; i < qntImg; i++) {
      let newDiv = document.querySelector('.best-images-img').cloneNode(true);
      newDiv.children[0].src = `Images/Best-images/Add-img/${j}-${i}.jpeg`;
      columns[i].appendChild(newDiv);
    }
  }
  document.querySelector('.best-images-btn').style.visibility = 'hidden';
}